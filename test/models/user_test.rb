require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.new(name: "Arkadiusz Buczek", email: "arkadiuszbuczek.phu@gmail.com", 
                     password: "123abc", password_confirmation: "123abc")
  end
  
  test "should be valid" do
    assert @user.valid?
  end
  
  test "name should be present" do
    @user.name = "     "
    assert_not @user.valid?
  end
  
  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end
  
  test "name shouldn't be too long" do
  @user.name = "a" * 51
  assert_not @user.valid?
  end
  
  test "email souldn't be too long" do
    @user.email = "a"*244 + "example@email.pl"
  end
  
  test "email format validation should accept correct adresses" do
    email_validation = %w[czeslaw@email.com wieslaw@gmail.com mietek@fmail.com]
    email_validation.each do |email_address|
      @user.email = email_address
      assert @user.email?, "#{email_address.inspect} should be valid"
    end
  end
  
  test "email format validation should reject incorrect adresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com foo@bar..com]
    invalid_addresses.each do |invalid_addresses|
      @user.email = invalid_addresses
      assert_not @user.valid?, "#{invalid_addresses.inspect} should be invalid"
    end
  end
  
  test "email address should be unique" do
    duplicated_user = @user.dup
    duplicated_user.email = @user.email.upcase
    @user.save
    assert_not duplicated_user.valid?
  end
  
  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end
  
  test "password shouldn't be blank" do
    @user.password = @user.password_confirmation = " " * 6
    assert_not @user.valid?
  end
  
  test "password shouldn't be shorter than 6" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end
  
  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?('')
  end
end
